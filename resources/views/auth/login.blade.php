@extends('layouts.app')
@section('content')
<div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5">
    <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
        <h2 class="card-title font-weight-bold mb-1">Welcome to Tiket Online! 👋</h2>
        <p class="card-text mb-2">Please sign-in to your account and start the adventure</p>
        <form class="auth-login-form mt-2" action="{{ route('login') }}" method="POST">
            @csrf
            <div class="form-group">
                <label class="form-label" for="login-email">Email</label>
                <input class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" type="email" name="email" required autocomplete="email" autofocus placeholder="{{ trans('global.login_email') }}" aria-describedby="login-email"  tabindex="1" value="{{ old('email', null) }}" />
                @if($errors->has('email'))
                    <div class="invalid-feedback">
                        {{ $errors->first('email') }}
                    </div>
                @endif
            </div>
            <div class="form-group">
                <div class="d-flex justify-content-between">
                    <label for="login-password">Password</label><a href="page-auth-forgot-password-v2.html"><small>Forgot Password?</small></a>
                </div>
                <div class="input-group input-group-merge form-password-toggle">
                    <input class="form-control form-control-merge {{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" type="password" name="password" placeholder="············" aria-describedby="login-password" tabindex="2" required  />
                    @if($errors->has('password'))
                        <div class="invalid-feedback">
                            {{ $errors->first('password') }}
                        </div>
                    @endif
                    <div class="input-group-append"><span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span></div>
                </div>
            </div>
            <div class="form-group">
                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" id="remember" type="checkbox" tabindex="3" />
                    <label class="custom-control-label" for="remember"> Remember Me</label>
                </div>
            </div>
            <button class="btn btn-primary btn-block" tabindex="4" type="submit">Sign in</button>
        </form>
    </div>
</div>
@endsection